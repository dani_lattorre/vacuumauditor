using System;
using Xunit;
using VacuumAuditor;
using System.Linq;
using VacuumAuditor.Model;
using VacuumAuditor.Infrastructure.Repositories;

namespace IntegrationTests.Repositories
{
    public class JobRepositoryTests : TestWithSqlite
    {
        private readonly IJobRepository jobRepository;
        private readonly Job job;

        public JobRepositoryTests(){
            jobRepository = new JobRepository(DbContext);
            job = new Job { Id = Guid.NewGuid(), HappenedAt = DateTime.Now, RobotId = Guid.NewGuid(), Reference = "xyz" };
        }

        [Fact]
        public void GivenExistingJobsWhenFindAllByRobotIdReturnThenTheJobsThatHaveTheRobotId()
        {
            jobRepository.Save(job);

            var jobs = jobRepository.FindAllByRobotId(job.RobotId);

            Assert.Contains(job, jobs);
        }

        [Fact]
        public void GivenANonPersistedJobWhenIsSavedThenIsCreated(){
            jobRepository.Save(job);

            var jobs = jobRepository.FindAll();
            Assert.Contains(job, jobs);
        }

        [Fact]
        public void GivenAPersistedJobWhenIsSavedThenIsUpdated(){
            jobRepository.Save(job);
            DateTime changedDateTime = job.HappenedAt.AddYears(1);
            job.HappenedAt = changedDateTime;

            jobRepository.Save(job);

            var jobs = jobRepository.FindAll();
            Assert.Single(jobs);
            Assert.Equal(changedDateTime, jobs.First().HappenedAt);
        }
    }
}
