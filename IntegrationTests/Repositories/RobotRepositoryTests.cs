using System;
using System.Linq;
using VacuumAuditor.Model;
using VacuumAuditor.Infrastructure.Repositories;
using Xunit;

namespace IntegrationTests.Repositories
{
    public class RobotRepositoryTests : TestWithSqlite
    {
        private readonly IRobotRepository robotRepository;
    
        
        public RobotRepositoryTests()
        {
            robotRepository = new RobotRepository(DbContext);
        }

        [Fact]
        public void GivenANonExistingRobotWhenIsSavedThenIsCreated()
        {
            Robot robot = new Robot { Id = Guid.NewGuid(), Reference = "irrelevant reference", Name = "irrelevant name" };

            robotRepository.Save(robot);

            Robot saved = robotRepository.FindWithReference(robot.Reference);
            Assert.Equal(robot, saved);
        }

        [Fact]
        public void GivenRobotByReference()
        {
            Robot robot = new Robot { Id = Guid.NewGuid(), Reference = "irrelevant reference", Name = "irrelevant name" };
            robotRepository.Save(robot);

            var robotDB = robotRepository.FindWithReference(robot.Reference);

            Assert.Equal(robot, robotDB);
        }
    }

}