using System;
using System.Linq;
using Xunit;

namespace IntegrationTests.Configurations
{
    public class JobConfigurationTest : TestWithSqlite
    {
        [Fact]
        public void TableShouldGetCreated()
        {
            Assert.False(DbContext.Jobs.Any());
        }
    }
}
