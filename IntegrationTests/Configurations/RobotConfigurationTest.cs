using System;
using System.Linq;
using Xunit;

namespace IntegrationTests.Configurations
{
    public class RobotConfigurationTest : TestWithSqlite
    {
        [Fact]
        public void TableShouldGetCreated()
        {
            Assert.False(DbContext.Robots.Any());
        }
    }
}
