﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VacuumAuditor.Migrations
{
    public partial class AddReferenceToJobs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "Jobs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Reference",
                table: "Jobs");
        }
    }
}
