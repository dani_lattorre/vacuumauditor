﻿using System;

namespace VacuumAuditor.Model
{
    public interface IRobotRepository
    {
        Robot FindWithReference(string robotReference);
        void Save(Robot robot);
    }
}
