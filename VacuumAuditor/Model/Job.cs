﻿using System;

namespace VacuumAuditor.Model
{
    public class Job
    {
        public Guid Id { get; set; }
        public Guid RobotId { get; set; }
        public string Reference { get; set; }
        public DateTime HappenedAt { get; set; }
    }
}
