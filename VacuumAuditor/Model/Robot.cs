﻿using System;

namespace VacuumAuditor.Model
{
    public class Robot
    {
        public Guid Id { get; set; }
        public string Reference { get; set; }
        public string Name { get; set; }
    }
}
