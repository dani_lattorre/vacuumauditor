using System;
using System.Collections.Generic;

namespace VacuumAuditor.Model
{
    public interface IJobRepository
    {
        IEnumerable<Job> FindAllByRobotId(Guid robotId);
        void Save(Job job);
        IEnumerable<Job> FindAll();
    }
}