﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VacuumAuditor.UseCases
{
    public class JobInvalidException : Exception
    {
        public JobInvalidException(IEnumerable<ValidationResult> validationResults): base("the job is invalid")
        {
            ValidationResults = validationResults;
        }

        public IEnumerable<ValidationResult> ValidationResults { get; }
    }
}
