﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VacuumAuditor.Model;

namespace VacuumAuditor.UseCases
{
    public class RegisterJob
    {
        private IJobRepository _jobRepository;
        private IRobotRepository _robotRepository;
        private IIdGenerator _idGenerator;

        public RegisterJob(IJobRepository jobRepository, IRobotRepository robotRepository, IIdGenerator idGenerator)
        {
            this._jobRepository = jobRepository;
            this._robotRepository = robotRepository;
            this._idGenerator = idGenerator;
        }

        public Job Execute(RegisterJobParameters parameters)
        {
            var validationResults = new List<ValidationResult>();
            bool IsValid = Validator.TryValidateObject(parameters, new ValidationContext(parameters), validationResults, true);
            if (!IsValid)
            {
                throw new JobInvalidException(validationResults);
            }

            var job = new Job { Id = _idGenerator.generate(), Reference = parameters.JobReference };

            var robot = _robotRepository.FindWithReference(parameters.RobotReference);            
            if (robot == null)
            {
                robot = new Robot { Id = _idGenerator.generate(), Reference = parameters.RobotReference, Name = parameters.RoboModelName };
                _robotRepository.Save(robot);
            }
            job.RobotId = robot.Id;

            _jobRepository.Save(job);

            return job;
        }
    }
}