using System;


namespace VacuumAuditor.UseCases
{
    public interface IIdGenerator
    {
        Guid generate();
    }
}
