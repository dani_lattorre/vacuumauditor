﻿using System;
using System.Collections.Generic;
using VacuumAuditor.Model;

namespace VacuumAuditor.UseCases
{
    public class GetJobs
    {
        private IJobRepository _jobRepository;

        public GetJobs(IJobRepository jobRepository)
        {
            this._jobRepository = jobRepository;
        }

        public IEnumerable<Job> Execute(string robotId)
        {
            return _jobRepository.FindAllByRobotId(new Guid(robotId));
        }
    }
}
