﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VacuumAuditor.UseCases
{
    public class RegisterJobParameters
    {
        [Required]
        public string RobotReference { get; set; }
        [Required]
        public string RoboModelName { get; set; }
        [Required]
        public string JobReference { get; set; }
        public DateTime JobHappenedAt { get; set; }
    }
}
