using VacuumAuditor.Model;
using System.Linq;

namespace VacuumAuditor.Infrastructure.Repositories
{
    public class RobotRepository : BaseRepository, IRobotRepository
    {

        public RobotRepository(VacuumAuditorDbContext context) : base(context)
        {

        }

        public Robot FindWithReference(string robotReference) => context.Robots.Where(robot => robot.Reference == robotReference).First();

        public void Save(Robot robot)
        {
            context.Robots.Add(robot);            
            context.SaveChanges();
        }

    }
}