using System;
using System.Collections.Generic;
using System.Linq;
using VacuumAuditor.Model;

namespace VacuumAuditor.Infrastructure.Repositories
{
    public class JobRepository : BaseRepository, IJobRepository
    {
        public JobRepository(VacuumAuditorDbContext context) : base(context)
        {
        }

        public IEnumerable<Job> FindAll()
        {
            return context.Jobs;
        }

        public IEnumerable<Job> FindAllByRobotId(Guid robotId) => context.Jobs.Where(job => job.RobotId == robotId);

        public void Save(Job job)
        {
            if(context.Jobs.Any(existingJob => existingJob.Id == job.Id)){
                context.Jobs.Update(job);
            }else{
                context.Jobs.Add(job);
            }
            context.SaveChanges();
        }
    }
}