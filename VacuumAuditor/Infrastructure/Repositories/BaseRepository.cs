using System;

namespace VacuumAuditor.Infrastructure.Repositories
{
    public abstract class BaseRepository : IDisposable
    {
        private bool _disposed = false;
        protected readonly VacuumAuditorDbContext context;

        public BaseRepository(VacuumAuditorDbContext context){
            this.context = context;
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}