﻿using System;
using Microsoft.EntityFrameworkCore;
using VacuumAuditor.Model;

namespace VacuumAuditor
{
    public class VacuumAuditorDbContext : DbContext
    {

        public DbSet<Job> Jobs { get; set; }
        public DbSet<Robot> Robots { get; set; }

        public VacuumAuditorDbContext(DbContextOptions<VacuumAuditorDbContext> options)
            : base(options)
        {
        }
    }

}
