﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace VacuumAuditor
{
    public class VacuumAuditorContextFactory : IDesignTimeDbContextFactory<VacuumAuditorDbContext>
    {
        public VacuumAuditorDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<VacuumAuditorDbContext>();
            optionsBuilder.UseSqlite("Data Source=auditor.db");

            return new VacuumAuditorDbContext(optionsBuilder.Options);
        }
    }
}
