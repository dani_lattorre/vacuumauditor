﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VacuumAuditor.Infrastructure.Repositories;
using VacuumAuditor.Model;
using VacuumAuditor.UseCases;
using DryIoc;

namespace VacuumAuditor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var options = new DbContextOptionsBuilder<VacuumAuditorDbContext>()
                   .UseSqlite("Data Source=auditor.db")
                   .Options;
            using (var db = new VacuumAuditorDbContext(options))
            {
                db.Database.EnsureCreated();

                Console.WriteLine("Inserting a new robot");
                db.Robots.Add(new Robot { Name = "Roomba iRobot", Reference = "XYZ" });
                db.SaveChanges();

                Console.WriteLine("Querying for a robot");
                var robot = db.Robots.First();

                var container = new Container(rules => rules.WithoutThrowOnRegisteringDisposableTransient());
                container.RegisterInstance(db);
                container.Register<IJobRepository, JobRepository>();
                container.Register<GetJobs>();

                IJobRepository jobRepository = container.Resolve<IJobRepository>();
                jobRepository.Save(new Job { Id = Guid.NewGuid(), RobotId = robot.Id, HappenedAt = DateTime.Now, Reference = "ABC" });

                GetJobs getJobs = container.Resolve<GetJobs>();

                Console.WriteLine("Getting jobs for: " + robot.Name);
                var jobs = getJobs.Execute(robot.Id.ToString());

                Console.WriteLine("This robot have performed " + jobs.Count() + " jobs");
            }
        }
    }
}
