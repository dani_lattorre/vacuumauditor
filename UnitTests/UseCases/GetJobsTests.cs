using System;
using Xunit;
using System.Collections.Generic;
using Moq;
using VacuumAuditor.Model;
using VacuumAuditor.UseCases;

namespace UnitTests.UseCases
{
    public class GetJobsTests
    {
        [Fact]
        public void GivenExistingJobsThenShouldBeReturned()
        {
            var job = new Job { RobotId = Guid.NewGuid(), Id = Guid.NewGuid(), HappenedAt = new DateTime() };
            var existingJobs = new List<Job>() { job };
            var jobRepositoryDouble = new Mock<IJobRepository>();
            jobRepositoryDouble.Setup(jobRepository => jobRepository.FindAllByRobotId(job.RobotId)).Returns(existingJobs);
            GetJobs getJobs = new GetJobs(jobRepositoryDouble.Object);

            var jobs = getJobs.Execute(job.RobotId.ToString());

            Assert.Contains(job, jobs);
        }
    }
}
