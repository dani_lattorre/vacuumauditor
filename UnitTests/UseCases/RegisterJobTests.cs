using System;
using System.Linq;
using Moq;
using VacuumAuditor.Model;
using VacuumAuditor.UseCases;
using Xunit;

namespace UnitTests.UseCases
{
    public class RegisterJobTests{
        readonly RegisterJob registerJob;
        readonly Mock<IJobRepository> jobRepositoryDouble;
        readonly Mock<IRobotRepository> robotRepositoryDouble;
        readonly Mock<IIdGenerator> idGeneratorDouble;
        readonly Robot existingRobot;
        Guid aGuid;

        public RegisterJobTests()
        {
            jobRepositoryDouble = new Mock<IJobRepository>();
            robotRepositoryDouble = new Mock<IRobotRepository>();
            idGeneratorDouble = new Mock<IIdGenerator>();
            existingRobot = new Robot { Id = Guid.NewGuid(), Reference = "13w4", Name = "Roomba iRobot" };
            robotRepositoryDouble.Setup(repository => repository.FindWithReference(existingRobot.Reference)).Returns(existingRobot);
            aGuid = Guid.NewGuid();
            idGeneratorDouble.Setup(idGenerator => idGenerator.generate()).Returns(aGuid);
            registerJob = new RegisterJob(jobRepositoryDouble.Object, robotRepositoryDouble.Object, idGeneratorDouble.Object);
        }

        [Fact]
        public void GivenAnExistingRobotAndAllParametersAreOkThenIsPersisted()
        {
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "xyz", JobHappenedAt = DateTime.Now, RobotReference = existingRobot.Reference, RoboModelName = existingRobot.Name };

            registerJob.Execute(parameters);

            jobRepositoryDouble.Verify(repository => repository.Save(It.IsAny<Job>()));
        }

        [Fact]
        public void GivenAnExistingRobotAndAllParametersAreOkThenIsAssignedToTheExistingRobot()
        {
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "xyz", JobHappenedAt = DateTime.Now, RobotReference = existingRobot.Reference, RoboModelName = existingRobot.Name };

            Job job = registerJob.Execute(parameters);

            Assert.Equal(existingRobot.Id, job.RobotId);
        }

        [Fact]
        public void GivenNonExistingRobotAndAllParametersAreOkThenTheRobotIsPersisted(){
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "xyz", JobHappenedAt = DateTime.Now, RobotReference = "foobar", RoboModelName = existingRobot.Name };

            registerJob.Execute(parameters);

            robotRepositoryDouble.Verify(repository => repository.Save(It.IsAny<Robot>()));
        }

        [Fact]
        public void GivenNonExistingRobotAndAllParametersAreOkThenIsAssignedToTheNewRobot()
        {
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "xyz", JobHappenedAt = DateTime.Now, RobotReference = "foobar", RoboModelName = existingRobot.Name };

            Job job = registerJob.Execute(parameters);

            Assert.Equal(aGuid, job.RobotId);
        }

        [Fact]
        public void GivenParametersDontReceiveJobReferenceThenFails()
        {            
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "", JobHappenedAt = DateTime.Now, RobotReference = existingRobot.Reference, RoboModelName = existingRobot.Name };

            Action action = () =>  registerJob.Execute(parameters);

            AssertJobInvalidException(action, "JobReference");
        }

        [Fact]
        public void GivenParametersDontReceiveRobotReferenceThenFails()
        {            
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "xyz", JobHappenedAt = DateTime.Now, RobotReference = "", RoboModelName = existingRobot.Name };

            Action action = () =>  registerJob.Execute(parameters);

            AssertJobInvalidException(action, "RobotReference");
        }

        [Fact]
        public void GivenParametersDontReceiveRobotNameThenFails()
        {            
            RegisterJobParameters parameters = new RegisterJobParameters { JobReference = "xyz", JobHappenedAt = DateTime.Now, RobotReference = "aaa", RoboModelName = "" };

            Action action = () =>  registerJob.Execute(parameters);

            AssertJobInvalidException(action, "RoboModelName");
        }

        private void AssertJobInvalidException(Action action, string memberName)
        {
            var exception = Assert.Throws<JobInvalidException>(action);
            Assert.Equal("the job is invalid", exception.Message);
            Assert.Single(exception.ValidationResults);
            Assert.Contains(memberName, exception.ValidationResults.First().MemberNames);
        }
    }
}