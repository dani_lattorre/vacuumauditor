﻿# Vacuum Cleaner Auditor

This is a hipotetical application to centralize the historic usages of a Robotic Vacuum Cleaner and it's maintenance costs. The purpose is practice TDD using test doubles.

## Infrastructure requirements

- .Net Core
- SQLite

## Testing

To build the application you need to compile:

```dotnet build```

You can run all the tests with
```dotnet test```

The unit tests only:

```dotnet test UnitTests```

The integration tests only:

```dotnet test IntegrationTests```

## Features

- [x] Show the jobs by the robot id
- [ ] Register robot jobs
    - [x] In each job are received the the job external reference, the job happend time, robot external reference and the robot model name 
    - [ ] All the fields are required
    - [ ] If a robot with the received external reference doesn't exist is created
- [ ] Show all the robots